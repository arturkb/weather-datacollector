package pl.arturkb.integration;

import pl.arturkb.integration.dto.CurrentWeatherReportDto;
import reactor.core.publisher.Mono;

/**
 * Interface for weather services.
 */
public interface WeatherServiceClient {


    /**
     * Gives current weather report by longitude and latitude.
     *
     * @param longitude, the longitude
     * @param latitude, the latitude
     * @return the current weather report.
     */
    Mono<CurrentWeatherReportDto> getCurrentWeatherReport(float longitude, float latitude);

}
