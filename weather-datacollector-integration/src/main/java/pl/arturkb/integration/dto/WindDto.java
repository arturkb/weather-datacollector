package pl.arturkb.integration.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@EqualsAndHashCode
@ToString
public class WindDto {

    private SpeedDto speed;

    /**
     * Wind direction, degrees (meteorological)
     */
    private double windDirection;

}
