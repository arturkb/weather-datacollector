package pl.arturkb.integration.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Builder
@EqualsAndHashCode
@Getter
@ToString
public class WeatherConditionsDto {

    /**
     * Internal representation of condition.
     */
    private int weatherConditionId;

    /**
     * Group of weather parameters (Rain, Snow, Extreme etc.)
     */
    private String conditions;

    /**
     * Weather condition within the group
     */
    private String description;

    /**
     * Weather icon id
     */
    private String iconId;
}
