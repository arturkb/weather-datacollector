package pl.arturkb.integration.dto;

import lombok.ToString;

@ToString
public enum VolumeTypeDto {

    /**
     * Volume for the last 3 hours
     */
    THREE_HOURS
}
