package pl.arturkb.integration.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * DTO that represents rain volume.
 */
@Builder
@Getter
@EqualsAndHashCode
@ToString
public class RainDto {

    private double volume;
    private VolumeTypeDto rainVolumeType;

}
