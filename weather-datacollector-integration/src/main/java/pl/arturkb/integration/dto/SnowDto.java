package pl.arturkb.integration.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * DTO that represents snow volume.
 */
@Builder
@Getter
@EqualsAndHashCode
@ToString
public class SnowDto {

    private double volume;
    private VolumeTypeDto volumeType;
}
