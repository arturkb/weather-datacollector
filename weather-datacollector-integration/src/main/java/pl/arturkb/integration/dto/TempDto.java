package pl.arturkb.integration.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
public class TempDto {

    private TempUnitDto tempUnit;
    private double value;

    /**
     * Temperature with default unit set to kalvin.
     *
     * @param value, Temperature value;
     */
    public TempDto(double value) {
        this.value = value;
        tempUnit = TempUnitDto.KELVIN;
    }

    /**
     * Temperature constructor for all parameters.
     *
     * @param tempUnit, the temp unit
     * @param value, the temp value
     */
    public TempDto(TempUnitDto tempUnit, double value) {
        this.tempUnit = tempUnit;
        this.value = value;
    }
}
