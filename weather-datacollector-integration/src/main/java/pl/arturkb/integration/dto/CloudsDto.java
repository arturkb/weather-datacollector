package pl.arturkb.integration.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * DTO class that represents cloudiness;
 */
@Builder
@Getter
@EqualsAndHashCode
@ToString
public class CloudsDto {

    /**
     * Cloudiness, %
     */
    private double cloudiness;

    private CloudinessTypeDto cloudinessType;
}

