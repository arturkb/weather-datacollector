package pl.arturkb.integration.dto;

import lombok.ToString;
import lombok.Value;

/**
 * DTO class that represents geographic coordinates for given point.
 */
@Value
@ToString
public class CoordinatesDto {

    /**
     * City geo location, longitude
     */
    float latitude;

    /**
     * City geo location, latitude
     */
    float longitude;
}