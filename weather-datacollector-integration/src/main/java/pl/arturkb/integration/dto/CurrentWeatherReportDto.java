package pl.arturkb.integration.dto;

import lombok.*;

import java.util.Date;
import java.util.List;

/**
 * DTO class that represents current weather report
 */
@Builder
@Getter
@EqualsAndHashCode
@ToString
public class CurrentWeatherReportDto {

    /**
     * Geographic coordinates for weather report.
     */
    private CoordinatesDto coordinates;
    private String cityName;
    private String country;

    private Date sunrise;
    private Date sunset;
    private Date timeOfDataCalculation;


    /**
     * Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa
     */
    private double  atmosphericPressure;

    /**
     * Humidity, %
     */
    private double humidity;

    /**
     * Minimum temperature at the moment. This is deviation from current temp that is possible for large cities
     * and megalopolises geographically expanded.
     */
    private TempDto  minimumTemperatureAtTheMoment;

    /**
     * Maximum temperature at the moment. This is deviation from current temp that is possible for large cities
     * and megalopolises geographically expanded.
     */
    private TempDto  maximumTemperatureAtTheMoment;

    private TempDto temperature;
    private WindDto wind;
    private CloudsDto cloudiness;
    private RainDto rainVolume;
    private SnowDto snowVolume;
    private int visibility;

    /**
     * Short representation of weather condition
     */
    @Singular(value = "weatherCondition")
    private List<WeatherConditionsDto> weatherConditions;
}
