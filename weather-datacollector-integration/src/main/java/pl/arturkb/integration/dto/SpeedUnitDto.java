package pl.arturkb.integration.dto;

import lombok.ToString;

@ToString
public enum SpeedUnitDto {
    METER_PER_SEC, KILOMETERS_PER_HOUR, MILES_HOUR
}
