package pl.arturkb.integration.dto;

import lombok.ToString;

@ToString
public enum TempUnitDto {
    KELVIN, CELSIUS, FAHRENHEIT
}
