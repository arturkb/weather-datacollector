package pl.arturkb.integration.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * SpeedDto class with speedUnit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
 */
@Getter
@EqualsAndHashCode
@ToString
public class SpeedDto {

    private double value;
    private SpeedUnitDto speedUnit;

    /**
     * SpeedDto with default unit sett to meter per sec.
     * If value is less then zero ten is sett to zero.
     *
     * @param value, Speed value
     */
    public SpeedDto(double value) {
        if (value < 0) {
            this.value = 0;
        } else {
            this.value = value;
        }
        speedUnit = SpeedUnitDto.METER_PER_SEC;
    }

    /**
     * SpeedDto constructor for all parameters.
     *
     * @param unit,  the SpeedUnit.
     * @param value, the value of speed.
     */
    public SpeedDto(SpeedUnitDto unit, double value) {

        if (value < 0) {
            this.value = 0;
        } else {
            this.value = value;
        }

        this.speedUnit = unit;
    }

}
