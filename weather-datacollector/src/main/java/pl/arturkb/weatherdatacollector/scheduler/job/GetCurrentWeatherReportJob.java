package pl.arturkb.weatherdatacollector.scheduler.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import pl.arturkb.integration.WeatherServiceClient;
import pl.arturkb.integration.dto.CurrentWeatherReportDto;
import pl.arturkb.weatherdatacollector.integration.openweathermap.service.OpenWeathermapClientImpl;
import reactor.core.publisher.Mono;

public class GetCurrentWeatherReportJob extends QuartzJobBean {

    private WeatherServiceClient weatherServiceClient = new OpenWeathermapClientImpl();

    private static final Logger logger
            = LoggerFactory.getLogger(GetCurrentWeatherReportJob.class);

    private String name;

    // Invoked if a Job data map entry with that name
    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        logger.info("Testing schedular");
        Mono<CurrentWeatherReportDto> currentWeatherReport = weatherServiceClient.getCurrentWeatherReport(19.45f, 51.61f);
        logger.info(currentWeatherReport.block().toString());
    }
}
