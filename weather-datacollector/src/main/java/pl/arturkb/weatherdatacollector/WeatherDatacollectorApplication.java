package pl.arturkb.weatherdatacollector;

import org.quartz.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.arturkb.weatherdatacollector.scheduler.job.GetCurrentWeatherReportJob;

@SpringBootApplication
public class WeatherDatacollectorApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeatherDatacollectorApplication.class, args);
    }


    @Bean
    public JobDetail getCurrentWeatherRaportJob() {
        return JobBuilder.newJob(GetCurrentWeatherReportJob.class).withIdentity("GetCurrentWeatherReportJob")
                .usingJobData("name", "Test").storeDurably().build();
    }

    @Bean
    public Trigger getCurrentWeatherRaportJobTrigger() {
        SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInHours(1).repeatForever();

        return TriggerBuilder.newTrigger().forJob(getCurrentWeatherRaportJob())
                .withIdentity("getCurrentWeatherRaportJobTrigger").withSchedule(scheduleBuilder).build();
    }
}
