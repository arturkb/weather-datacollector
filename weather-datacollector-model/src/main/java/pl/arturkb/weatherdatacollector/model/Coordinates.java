package pl.arturkb.weatherdatacollector.model;

import lombok.Value;

/**
 * Class that represents geographic coordinates for given point.
 */
@Value
public class Coordinates {

    /**
     * City geo location, longitude
     */
    float latitude;

    /**
     * City geo location, latitude
     */
    float longitude;
}
