package pl.arturkb.weatherdatacollector.model;

import lombok.*;

import java.util.Date;

@Builder
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
public class CurrentWeatherReport {

    /**
     * Date and time for creation of report.
     */
    private Date creationTime;

    /**
     * Geographic coordinates for weather report.
     */
    private Coordinates coordinates;

}
