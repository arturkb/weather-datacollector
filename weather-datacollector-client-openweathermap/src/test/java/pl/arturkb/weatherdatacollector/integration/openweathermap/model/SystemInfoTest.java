package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SystemInfoTest {

    @Test
    void testDeserialization() throws IOException {

        SystemInfo fixture = SystemInfo.builder()
                .type(1)
                .id(5358)
                .message(0.0065f)
                .country("PL")
                .sunrise(1530671462)
                .sunset(1530730910)
                .build();

        String serializedClass = "{\n" +
                "        \"type\": 1,\n" +
                "        \"id\": 5358,\n" +
                "        \"message\": 0.0065,\n" +
                "        \"country\": \"PL\",\n" +
                "        \"sunrise\": 1530671462,\n" +
                "        \"sunset\": 1530730910\n" +
                "    }";

        SystemInfo deserializedClass = new ObjectMapper().readValue(serializedClass, SystemInfo.class);
        assertEquals(fixture, deserializedClass);
    }
}
