package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WindTest {

    @Test
    void testDeserialization() throws IOException {
        Wind fixture = Wind.builder().speed(new Speed(2.6)).direction(280).build();
        String serializedClass = " {\n" +
                "        \"speed\": 2.6,\n" +
                "        \"deg\": 280\n" +
                "    }";
        Wind deserializedClass = new ObjectMapper().readValue(serializedClass, Wind.class);
        assertEquals(fixture, deserializedClass);
    }
}
