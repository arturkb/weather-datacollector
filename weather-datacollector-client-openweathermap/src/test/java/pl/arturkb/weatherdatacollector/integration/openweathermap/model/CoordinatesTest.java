package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CoordinatesTest {

    @Test
    void testDeserialization() throws IOException {
        Coordinates fixture = new Coordinates(139L, 35L);
        String serializedClass = "{\"lat\":139,\"lon\":35}";
        Coordinates deserializedClass = new ObjectMapper().readValue(serializedClass, Coordinates.class);
        assertEquals(fixture, deserializedClass);
    }

}
