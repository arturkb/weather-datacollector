package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MainWeatherTest {

    @Test
    void testDeserialization() throws IOException {
        MainWeather fixture = MainWeather.builder()
                .temp(new Temp(291.15))
                .pressure(1014)
                .humidity(59)
                .tempMin(new Temp(291.15))
                .tempMax(new Temp(291.15))
                .build();
        String serializedClass = "{\n" +
                "        \"temp\": 291.15,\n" +
                "        \"pressure\": 1014,\n" +
                "        \"humidity\": 59,\n" +
                "        \"temp_min\": 291.15,\n" +
                "        \"temp_max\": 291.15\n" +
                "    }";
        MainWeather deserializedClass = new ObjectMapper().readValue(serializedClass, MainWeather.class);
        assertEquals(fixture, deserializedClass);
    }
}
