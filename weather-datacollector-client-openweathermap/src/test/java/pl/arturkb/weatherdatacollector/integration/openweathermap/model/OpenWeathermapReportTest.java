package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OpenWeathermapReportTest {

    /**
     * This is test that reproduce response from real call like this
     * api.openweathermap.org/data/2.5/weather?lat=51.61&lon=19.45&APPID=9f1f736a16e4068a68021f77e47bbdf6
     */
    @Test
    void testDeserializationForRealCall() throws IOException {
        String serializedClass = jsonResponse();
        OpenWeathermapReport fixture = buildModelObject();

        OpenWeathermapReport deserializedClass = new ObjectMapper().readValue(serializedClass, OpenWeathermapReport.class);
        assertEquals(fixture, deserializedClass);
    }

    private OpenWeathermapReport buildModelObject() {
        return OpenWeathermapReport.builder()
                .coordinates(new Coordinates(51.61f, 19.45f))
                .weather(Arrays.asList(Weather.builder()
                        .id(800)
                        .main("Clear").description("clear sky")
                        .icon("01d")
                        .build())
                )
                .base("stations")
                .mainWeather(MainWeather.builder().temp(new Temp(291.15)).pressure(1014).humidity(59)
                        .tempMin(new Temp(291.15)).tempMax(new Temp(291.15)).build())
                .visibility(10000)
                .wind(Wind.builder().speed(new Speed(2.6)).direction(280).build())
                .clouds(new Clouds(0))
                .dt(1530682200)
                .sys(SystemInfo.builder().type(1).id(5358).message(0.0065f).country("PL")
                        .sunrise(1530671462).sunset(1530730910).build())
                .id(3082945)
                .name("Tuszyn")
                .cod(200)
                .build();
    }

    private String jsonResponse() {
        return "{\n" +
                "    \"coord\": {\n" +
                "        \"lon\": 19.45,\n" +
                "        \"lat\": 51.61\n" +
                "    },\n" +
                "    \"weather\": [\n" +
                "        {\n" +
                "            \"id\": 800,\n" +
                "            \"main\": \"Clear\",\n" +
                "            \"description\": \"clear sky\",\n" +
                "            \"icon\": \"01d\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"base\": \"stations\",\n" +
                "    \"main\": {\n" +
                "        \"temp\": 291.15,\n" +
                "        \"pressure\": 1014,\n" +
                "        \"humidity\": 59,\n" +
                "        \"temp_min\": 291.15,\n" +
                "        \"temp_max\": 291.15\n" +
                "    },\n" +
                "    \"visibility\": 10000,\n" +
                "    \"wind\": {\n" +
                "        \"speed\": 2.6,\n" +
                "        \"deg\": 280\n" +
                "    },\n" +
                "    \"clouds\": {\n" +
                "        \"all\": 0\n" +
                "    },\n" +
                "    \"dt\": 1530682200,\n" +
                "    \"sys\": {\n" +
                "        \"type\": 1,\n" +
                "        \"id\": 5358,\n" +
                "        \"message\": 0.0065,\n" +
                "        \"country\": \"PL\",\n" +
                "        \"sunrise\": 1530671462,\n" +
                "        \"sunset\": 1530730910\n" +
                "    },\n" +
                "    \"id\": 3082945,\n" +
                "    \"name\": \"Tuszyn\",\n" +
                "    \"cod\": 200\n" +
                "}";
    }
}
