package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CloudsTest {

    @Test
    void testDeserialization() throws IOException {
        Clouds fixture = new Clouds(0);
        String serializedClass = "{\n" +
                "        \"all\": 0\n" +
                "    }";
        Clouds deserializedClass = new ObjectMapper().readValue(serializedClass, Clouds.class);
        assertEquals(fixture, deserializedClass);
    }
}
