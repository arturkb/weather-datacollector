package pl.arturkb.weatherdatacollector.integration.openweathermap.transform;

import org.junit.jupiter.api.Test;
import pl.arturkb.integration.dto.*;
import pl.arturkb.weatherdatacollector.integration.openweathermap.model.*;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class OpenWeathermapReportTransformerTest {

    private static final float latitude = 1.0f;
    private static final float longitude = 2.0f;
    private static final int weatherId = 1;
    private static final String weatherMain = "Clouds";
    private static final String weatherDescription = "few clouds";
    private static final String weatherIconId = "02d";
    private static final double mainWeatherTempValue = 302.15d;
    private static final double mainWeatherPressure = 1019d;
    private static final double mainWeatherHumidity = 74d;
    private static final double windSpeedValue = 5.1d;
    private static final double windDirection = 150d;
    private static final double cloudiness = 75d;
    private static final String sysCountry = "PL";
    private static final long sysSunrise = 1530757923;
    private static final long sysSunset = 1530817272;
    private static final long dt = 1530809316;


    @Test
    void testConvertToDtoHappyDay() {

        OpenWeathermapReport openWeathermapReport = createReport(true);
        Mono<OpenWeathermapReport> monoModel = Mono.just(openWeathermapReport);
        Mono<CurrentWeatherReportDto> response = new OpenWeathermapReportTransformer().convertToDto(monoModel);
        CurrentWeatherReportDto fixture = createReportDto(true);

        StepVerifier.create(response)
                .consumeNextWith(currentWeatherReportDto -> assertEquals(fixture, currentWeatherReportDto))
                .verifyComplete();
    }

    @Test
    void testConvertToDtoShouldNotThrowNPE() {

        OpenWeathermapReport openWeathermapReport = createReport(false);
        Mono<OpenWeathermapReport> monoModel = Mono.just(openWeathermapReport);
        Mono<CurrentWeatherReportDto> response = new OpenWeathermapReportTransformer().convertToDto(monoModel);
        CurrentWeatherReportDto fixture = createReportDto(false);

        StepVerifier.create(response)
                .consumeNextWith(currentWeatherReportDto -> assertEquals(fixture, currentWeatherReportDto))
                .verifyComplete();
    }

    @Test
    void testConvertToDtoShouldThrowNPE() {
        assertThrows(NullPointerException.class, () -> OpenWeathermapReport.builder().build());
    }

    private OpenWeathermapReport createReport(boolean includeMaxMinTemp) {
        float sysMessage = 0.0344f;
        int sysType = 1;
        int sysId = 1;
        String baseType = "stations";
        return OpenWeathermapReport.builder()
                .coordinates(new Coordinates(latitude, longitude))
                .weather(Arrays.asList(Weather.builder().id(weatherId).main(weatherMain).description(weatherDescription).icon(weatherIconId).build()))
                .base(baseType)
                .dt(dt)
                .mainWeather(MainWeather.builder().temp(new Temp(mainWeatherTempValue)).pressure(mainWeatherPressure).humidity(mainWeatherHumidity)
                        .tempMin(includeMaxMinTemp ? new Temp(mainWeatherTempValue) : null)
                        .tempMax(includeMaxMinTemp ? new Temp(mainWeatherTempValue) : null).build())
                .wind(Wind.builder().speed(new Speed(windSpeedValue)).direction(windDirection).build())
                .clouds(new Clouds(cloudiness))
                .sys(SystemInfo.builder().id(sysId).type(sysType).message(sysMessage).country(sysCountry).sunrise(sysSunrise).sunset(sysSunset).build())
                .build();

    }

    private CurrentWeatherReportDto createReportDto(boolean includeMaxMinTemp) {
        return CurrentWeatherReportDto.builder()
                .coordinates(new CoordinatesDto(latitude, longitude))
                .weatherCondition(WeatherConditionsDto.builder()
                        .weatherConditionId(weatherId)
                        .conditions(weatherMain)
                        .description(weatherDescription)
                        .iconId(weatherIconId)
                        .build())
                .temperature(new TempDto(mainWeatherTempValue))
                .atmosphericPressure(mainWeatherPressure)
                .humidity(mainWeatherHumidity)
                .maximumTemperatureAtTheMoment(includeMaxMinTemp ? new TempDto(mainWeatherTempValue) : null)
                .minimumTemperatureAtTheMoment(includeMaxMinTemp ? new TempDto(mainWeatherTempValue) : null)
                .wind(WindDto.builder().speed(new SpeedDto(windSpeedValue)).windDirection(windDirection).build())
                .cloudiness(CloudsDto.builder().cloudiness(cloudiness).cloudinessType(CloudinessTypeDto.ALL).build())
                .country(sysCountry)
                .sunrise(new Date(sysSunrise))
                .sunset(new Date(sysSunset))
                .timeOfDataCalculation(new Date(dt))
                .build();
    }

}
