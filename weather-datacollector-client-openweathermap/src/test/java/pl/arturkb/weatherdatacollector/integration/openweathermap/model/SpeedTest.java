package pl.arturkb.weatherdatacollector.integration.openweathermap.model;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SpeedTest {

    private Speed speed;


    @Test
    void shouldReturnTheSameValue() {
        final double value = 100;
        speed = new Speed(SpeedUnit.METER_PER_SEC, value);
        assertEquals(SpeedUnit.METER_PER_SEC, speed.getUnit());
        assertEquals(Double.valueOf(value), Double.valueOf(speed.getValue()));
    }

    @Test
    void shouldConvertDefaultUnitToKmPerHour() {
        final double value = 15;
        /* TWe use default unit that is meter per sec */
        speed = new Speed(value);
        double kmPerHour = value * 3.6d;
        Speed expectedSpeed = new Speed(SpeedUnit.KILOMETERS_PER_HOUR, kmPerHour);
        assertEquals(expectedSpeed, speed.convertToKilometersPerHour());
    }

    @Test
    void shouldNotConvertToKmPerHour() {
        final double value = 15;
        speed = new Speed(SpeedUnit.MILES_HOUR, value);
        assertEquals(speed, speed.convertToKilometersPerHour());
    }

    @Test
    void creatingSpeedWithNegativeValueShouldGiveZeroValue() {
        final double value = -15;
        speed = new Speed(SpeedUnit.MILES_HOUR, value);
        assertEquals(Double.valueOf(speed.getValue()), Double.valueOf(0));
    }

    @Test
    void creatingSpeedWithDefaultUnitAndNegativeValueShouldGiveZeroValue() {
        final double value = -15;
        speed = new Speed(value);
        assertEquals(Double.valueOf(speed.getValue()), Double.valueOf(0));
    }
}
