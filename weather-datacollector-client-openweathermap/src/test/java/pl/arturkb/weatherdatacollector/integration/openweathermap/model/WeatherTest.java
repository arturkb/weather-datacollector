package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WeatherTest {

    @Test
    void testDeserialization() throws IOException {
        Weather fixture = Weather.builder()
                .id(803)
                .main("Clouds")
                .description("broken clouds")
                .icon("04n")
                .build();

        String serializedClass = "{\"id\":\"803\",\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}";
        Weather deserializedClass = new ObjectMapper().readValue(serializedClass, Weather.class);
        assertEquals(fixture, deserializedClass);
    }
}
