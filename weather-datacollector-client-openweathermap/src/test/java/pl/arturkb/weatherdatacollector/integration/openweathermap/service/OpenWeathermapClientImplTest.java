package pl.arturkb.weatherdatacollector.integration.openweathermap.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.arturkb.integration.WeatherServiceClient;
import pl.arturkb.integration.dto.*;
import pl.arturkb.weatherdatacollector.integration.openweathermap.model.*;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.function.Consumer;

import static org.junit.Assert.assertEquals;

class OpenWeathermapClientImplTest {

    private MockWebServer mockWebServer;
    private WeatherServiceClient openWeathermapClient;

    private static final float latitude = 1.0f;
    private static final float longitude = 2.0f;
    private static final int weatherId = 1;
    private static final String weatherMain = "Clouds";
    private static final String weatherDescription = "few clouds";
    private static final String weatherIconId = "02d";
    private static final double mainWeatherTempValue = 302.15d;
    private static final double mainWeatherPressure = 1019d;
    private static final double mainWeatherHumidity = 74d;
    private static final double windSpeedValue = 5.1d;
    private static final double windDirection = 150d;
    private static final double cloudiness = 75d;
    private static final String sysCountry = "PL";
    private static final long sysSunrise = 1530757923;
    private static final long sysSunset = 1530817272;
    private static final long dt = 1530809316;

    @BeforeEach
    void setup() {
        mockWebServer = new MockWebServer();
        openWeathermapClient = new OpenWeathermapClientImpl(mockWebServer.url("/").toString());
    }

    @AfterEach
    void shutdown() throws IOException {
        mockWebServer.shutdown();
    }

    @Test
    void shouldReceiveCurrentWeatherReport() throws JsonProcessingException {

        OpenWeathermapReport openWeathermapReport = createReport(true);
        String serialized = new ObjectMapper().writeValueAsString(openWeathermapReport);
        prepareResponse(mockResponse -> mockResponse
                .setHeader("Content-Type", "application/json")
                .setBody(serialized));

        Mono<CurrentWeatherReportDto> weatherReportDto = openWeathermapClient.getCurrentWeatherReport(longitude, latitude);

        CurrentWeatherReportDto fixture = createReportDto(true);

        StepVerifier.create(weatherReportDto)
                .expectNext(fixture)
                .expectComplete().log().verify(Duration.ofSeconds(3));

        assertEquals(1, mockWebServer.getRequestCount());
    }

    private void prepareResponse(Consumer<MockResponse> consumer) {
        MockResponse mockResponse = new MockResponse();
        consumer.accept(mockResponse);
        mockWebServer.enqueue(mockResponse);
    }

    private OpenWeathermapReport createReport(boolean includeMaxMinTemp) {
        float sysMessage = 0.0344f;
        int sysType = 1;
        int sysId = 1;
        String baseType = "stations";
        return OpenWeathermapReport.builder()
                .coordinates(new Coordinates(latitude, longitude))
                .weather(Arrays.asList(Weather.builder().id(weatherId).main(weatherMain).description(weatherDescription).icon(weatherIconId).build()))
                .base(baseType)
                .dt(dt)
                .mainWeather(MainWeather.builder().temp(new Temp(mainWeatherTempValue)).pressure(mainWeatherPressure).humidity(mainWeatherHumidity)
                        .tempMin(includeMaxMinTemp ? new Temp(mainWeatherTempValue) : null)
                        .tempMax(includeMaxMinTemp ? new Temp(mainWeatherTempValue) : null).build())
                .wind(Wind.builder().speed(new Speed(windSpeedValue)).direction(windDirection).build())
                .clouds(new Clouds(cloudiness))
                .sys(SystemInfo.builder().id(sysId).type(sysType).message(sysMessage).country(sysCountry).sunrise(sysSunrise).sunset(sysSunset).build())
                .build();

    }

    private CurrentWeatherReportDto createReportDto(boolean includeMaxMinTemp) {
        return CurrentWeatherReportDto.builder()
                .coordinates(new CoordinatesDto(latitude, longitude))
                .weatherCondition(WeatherConditionsDto.builder()
                        .weatherConditionId(weatherId)
                        .conditions(weatherMain)
                        .description(weatherDescription)
                        .iconId(weatherIconId)
                        .build())
                .temperature(new TempDto(mainWeatherTempValue))
                .atmosphericPressure(mainWeatherPressure)
                .humidity(mainWeatherHumidity)
                .maximumTemperatureAtTheMoment(includeMaxMinTemp ? new TempDto(mainWeatherTempValue) : null)
                .minimumTemperatureAtTheMoment(includeMaxMinTemp ? new TempDto(mainWeatherTempValue) : null)
                .wind(WindDto.builder().speed(new SpeedDto(windSpeedValue)).windDirection(windDirection).build())
                .cloudiness(CloudsDto.builder().cloudiness(cloudiness).cloudinessType(CloudinessTypeDto.ALL).build())
                .country(sysCountry)
                .sunrise(new Date(sysSunrise))
                .sunset(new Date(sysSunset))
                .timeOfDataCalculation(new Date(dt))
                .build();
    }

    void expectRequest(Consumer<RecordedRequest> consumer) {
        try {
            consumer.accept(mockWebServer.takeRequest());
        } catch (InterruptedException ex) {
            throw new IllegalStateException(ex);
        }
    }

}
