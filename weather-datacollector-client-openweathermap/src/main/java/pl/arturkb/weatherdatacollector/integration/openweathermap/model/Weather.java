package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import lombok.*;

/**
 * Weather condition codes
 */
@Builder
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
public class Weather {

    /**
     * Weather condition id
     */
    private int id;

    /**
     * Group of weather parameters (Rain, Snow, Extreme etc.)
     */
    private String main;

    /**
     * Weather condition within the group
     */
    private String description;

    /**
     * Weather icon id
     */
    private String icon;
}
