package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

/**
 * Domain class for API {@link "https://openweathermap.org/current"}
 */
@Builder
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
public class OpenWeathermapReport {

    @JsonProperty("coord")
    @NonNull
    private Coordinates coordinates;

    /**
     * Weather condition codes
     */
    @NonNull
    private List<Weather> weather;

    /**
     * Internal parameter
     */
    private String base;

    @JsonProperty("main")
    @NonNull
    private MainWeather mainWeather;

    private int visibility;

    @NonNull
    private Wind wind;

    @NonNull
    private Clouds clouds;

    /**
     * Time of data calculation, unix, UTC
     */
    private long dt;

    @NonNull
    private SystemInfo sys;

    /**
     * City ID
     */
    private int id;

    /**
     * City name
     */
    private String name;

    /**
     * Internal parameter
     */
    private int cod;
}
