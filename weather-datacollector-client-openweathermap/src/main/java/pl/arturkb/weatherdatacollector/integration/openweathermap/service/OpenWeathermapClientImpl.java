package pl.arturkb.weatherdatacollector.integration.openweathermap.service;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import pl.arturkb.integration.WeatherServiceClient;
import pl.arturkb.integration.dto.CurrentWeatherReportDto;
import pl.arturkb.weatherdatacollector.integration.openweathermap.model.OpenWeathermapReport;
import pl.arturkb.weatherdatacollector.integration.openweathermap.transform.OpenWeathermapReportTransformer;
import reactor.core.publisher.Mono;

/**
 * REST client for {@link "https://openweathermap.org"}
 */
@Service
public class OpenWeathermapClientImpl implements WeatherServiceClient {

    private final WebClient webClient;

    @Value("${openWeathermap.api.key}")
    private String apiKey;

    @Value("$openWeathermap.{service.url]")
    private String serviceUrl;

    public OpenWeathermapClientImpl(String url) {
        webClient = WebClient.create(url);
    }

    public OpenWeathermapClientImpl() {
        webClient = WebClient.create(serviceUrl);
    }

    @Override
    public Mono<CurrentWeatherReportDto> getCurrentWeatherReport(float longitude, float latitude) {

        Mono<OpenWeathermapReport> result = webClient.get()
                .uri("/data/2.5/weather?lat=" + latitude + "&lon=" + longitude + "&APPID=" + apiKey)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(OpenWeathermapReport.class);

        return new OpenWeathermapReportTransformer().convertToDto(result);
    }
}
