package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

public enum SpeedUnit {

    METER_PER_SEC, KILOMETERS_PER_HOUR, MILES_HOUR;

    double convertMeterPerSecToKilometersPerHour(double speed) {
        if (this.equals(METER_PER_SEC)) {
            return (speed * 3.6d);
        } else {
            return 0;
        }
    }


}
