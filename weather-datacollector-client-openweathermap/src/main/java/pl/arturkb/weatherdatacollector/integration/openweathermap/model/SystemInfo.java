package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import lombok.*;

@Builder
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
public class SystemInfo {

    /**
     * Internal parameter
     */
    private int type;

    /**
     * Internal parameter
     */
    private int id;

    /**
     * Internal parameter
     */
    private float message;

    /**
     * Country code (GB, JP etc.)
     */
    private String country;

    /**
     * Sunrise time, unix, UTC
     */
    private long sunrise;

    /**
     * Sunset time, unix, UTC
     */
    private long sunset;

}
