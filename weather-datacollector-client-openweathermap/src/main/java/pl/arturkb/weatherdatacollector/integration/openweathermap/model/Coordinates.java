package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Getter
@EqualsAndHashCode
public class Coordinates {

    /**
     * City geo location, longitude
     */
    @JsonProperty("lat")
    float latitude;

    /**
     * City geo location, latitude
     */
    @JsonProperty("lon")
    float longitude;

}
