package pl.arturkb.weatherdatacollector.integration.openweathermap.transform;

import pl.arturkb.integration.dto.*;
import pl.arturkb.weatherdatacollector.integration.openweathermap.model.OpenWeathermapReport;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.stream.Collectors;

public class OpenWeathermapReportTransformer {

    public Mono<CurrentWeatherReportDto> convertToDto(Mono<OpenWeathermapReport> monoModel) {
        return monoModel.map(openWeathermapReport -> CurrentWeatherReportDto.builder()
                .coordinates(new CoordinatesDto(openWeathermapReport.getCoordinates().getLatitude(), openWeathermapReport.getCoordinates().getLongitude()))
                .weatherConditions(
                        openWeathermapReport.getWeather().stream().map(
                                weather -> WeatherConditionsDto.builder()
                                        .weatherConditionId(weather.getId())
                                        .conditions(weather.getMain())
                                        .description(weather.getDescription())
                                        .iconId(weather.getIcon())
                                        .build())
                                .collect(Collectors.toList()))
                .temperature(new TempDto(openWeathermapReport.getMainWeather().getTemp().getValue()))
                .atmosphericPressure(openWeathermapReport.getMainWeather().getPressure())
                .humidity(openWeathermapReport.getMainWeather().getHumidity())
                .maximumTemperatureAtTheMoment(
                        openWeathermapReport.getMainWeather().getTempMax() != null ?
                                new TempDto(openWeathermapReport.getMainWeather().getTempMax().getValue())
                                : null)
                .minimumTemperatureAtTheMoment(openWeathermapReport.getMainWeather().getTempMin() != null ?
                                new TempDto(openWeathermapReport.getMainWeather().getTempMin().getValue())
                                : null)
                .visibility(openWeathermapReport.getVisibility())
                .wind(WindDto.builder().speed(new SpeedDto(openWeathermapReport.getWind().getSpeed().getValue()))
                        .windDirection(openWeathermapReport.getWind().getDirection()).build())
                .cloudiness(CloudsDto.builder().cloudinessType(CloudinessTypeDto.ALL)
                        .cloudiness(openWeathermapReport.getClouds().getCloudiness()).build())
                .timeOfDataCalculation(new Date(openWeathermapReport.getDt()))
                .country(openWeathermapReport.getSys().getCountry())
                .sunrise(new Date(openWeathermapReport.getSys().getSunrise()))
                .sunset(new Date(openWeathermapReport.getSys().getSunset()))
                .cityName(openWeathermapReport.getName())
                .build());
    }
}
