package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

public enum TempUnit {

    KELVIN, CELSIUS, FAHRENHEIT

}
