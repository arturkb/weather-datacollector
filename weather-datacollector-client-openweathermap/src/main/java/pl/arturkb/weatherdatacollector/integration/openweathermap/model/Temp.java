package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
public class Temp {

    @JsonIgnore
    private TempUnit unit;

    @JsonProperty("temp")
    private double value;

    /**
     * Temperature with default unit set to kalvin.
     *
     * @param value, Temperature value;
     */
    public Temp(double value) {
        this.value = value;
        unit = TempUnit.KELVIN;
    }

    /**
     * Temperature constructor for all parameters.
     *
     * @param unit, the temp unit
     * @param value, the temp value
     */
    public Temp(TempUnit unit, double value) {
        this.unit = unit;
        this.value = value;
    }
}
