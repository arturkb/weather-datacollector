package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
public class MainWeather {

    /**
     * Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
     */
    @NonNull
    private Temp temp;

    /**
     * Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa
     */
    private double pressure;

    /**
     * Humidity, %
     */
    private double humidity;

    /**
     * Minimum temperature at the moment. This is deviation from current temp that is possible for large cities
     * and megalopolises geographically expanded (use these parameter optionally). Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
     */
    @JsonProperty("temp_min")
    private Temp tempMin;


    /**
     * Maximum temperature at the moment. This is deviation from current temp that is possible for large cities
     * and megalopolises geographically expanded (use these parameter optionally). Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
     */
    @JsonProperty("temp_max")
    private Temp tempMax;

    /**
     * Atmospheric pressure on the sea level, hPa
     */
    @JsonProperty("sea_level")
    private double seaLevel;


    /**
     * Atmospheric pressure on the ground level, hPa
     */
    @JsonProperty("grnd_level")
    private double grndLevel;

}
