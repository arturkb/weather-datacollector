package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class Clouds {

    /**
     * Cloudiness, %
     */
    @JsonProperty("all")
    private double cloudiness;

}
