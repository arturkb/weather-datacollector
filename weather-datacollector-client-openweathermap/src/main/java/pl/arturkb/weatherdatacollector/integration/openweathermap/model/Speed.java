package pl.arturkb.weatherdatacollector.integration.openweathermap.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
public class Speed {

    @JsonIgnore
    private SpeedUnit unit;

    @JsonProperty("value")
    private double value;

    /**
     * Speed with default unit sett to meter per sec.
     * If value is less then zero ten is sett to zero.
     *
     * @param value, Speed value
     */
    public Speed(double value) {
        if (value < 0) {
            this.value = 0;
        } else {
            this.value = value;
        }
        unit = SpeedUnit.METER_PER_SEC;
    }

    /**
     * Speed constructor for all parameters.
     *
     * @param unit,  the SpeedUnit.
     * @param value, the value of speed.
     */
    public Speed(SpeedUnit unit, double value) {

        if (value < 0) {
            this.value = 0;
        } else {
            this.value = value;
        }

        this.unit = unit;
    }

    /**
     * Converts existing value unit to kilometer per second
     *
     * @return return new converted object with value in kilometer per second.
     */
    public Speed convertToKilometersPerHour() {
        Speed newSpeed;

        switch (unit) {
            case METER_PER_SEC:
                newSpeed = new Speed(SpeedUnit.KILOMETERS_PER_HOUR, unit.convertMeterPerSecToKilometersPerHour(value));
                break;

            default:
                newSpeed = this;
        }

        return newSpeed;
    }


}
